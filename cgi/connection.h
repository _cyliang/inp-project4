#pragma once
#include "job.h"
#include <netinet/in.h>
#include <fstream>
#include <string>

class Connection {
public:
	Connection(JobList &jobList, int id, const char *host, unsigned short port, const char *file, const char *proxyHost, unsigned short proxyPort);

private:
	void pushConnect();
	static void pushConnectCallback(void *, ssize_t);
	void pushRequestSock();
	static void pushRequestSockCallback(void *, ssize_t);
	void pushSockResponse();
	static void pushSockResponseCallback(void *, ssize_t);
	void pushWrite();
	static void pushWriteCallback(void *, ssize_t);
	void pushRead();
	static void pushReadCallback(void *, ssize_t);
	void print(const char *, bool strong = false);

	struct SOCK {
		unsigned char VN;
		unsigned char CD;
		unsigned short port;
		unsigned char ip[4];
		unsigned char nul;
	} connectSock;

	bool useSock;
	const int id;
	JobList &jobList;
	int sockFd;
	sockaddr_in addr;
	std::ifstream file;
	std::string fileLine;
	char readBuf[10000];
};
