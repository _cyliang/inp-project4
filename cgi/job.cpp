#include "job.h"
#include <fcntl.h>
#include <unistd.h>
#include <sys/select.h>
#include <cstdio>
using namespace std;

JobList::JobList():
	maxFd(0)
{
}

void JobList::pushReadJob(int fd, void *buf, size_t maxRead, Callback callback, void *callbackArg) {
	pushJob(
		Job::READ,
		fd, buf, maxRead, callback, callbackArg
	);
}

void JobList::pushWriteJob(int fd, void *buf, size_t n, Callback callback, void *callbackArg) {
	pushJob(
		Job::WRITE,
		fd, buf, n, callback, callbackArg
	);
}

void JobList::pushJob(Job::Type type, int fd, void *buf, size_t n, Callback callback, void *callbackArg) {
	Job newJob;

	newJob.type = type;
	newJob.fd = fd;
	newJob.callback = callback;
	newJob.callbackArg = callbackArg;
	newJob.buf = buf;
	newJob.n = n;
	newJob.total = 0;

	int fflag = fcntl(fd, F_GETFL, 0);
	fcntl(fd, F_SETFL, fflag | O_NONBLOCK);

	jobList.push_front(newJob);
	if(fd > maxFd)
		maxFd = fd;
}

int JobList::setCheckSet(fd_set *rSet, fd_set *wSet) {
	FD_ZERO(rSet);
	FD_ZERO(wSet);

	for(list<Job>::iterator it = jobList.begin(); it != jobList.end(); ) {
		if(fcntl(it->fd, F_GETFD) == -1) {
			jobList.erase(it++);
			continue;
		}
		
		if(it->type == Job::READ)
			FD_SET(it->fd, rSet);
		else
			FD_SET(it->fd, wSet);
		++it;
	}

	return jobList.size();
}

void JobList::checkDone(fd_set *rSet, fd_set *wSet) {
	for(list<Job>::iterator it = jobList.begin(); it != jobList.end(); ) {
		ssize_t n = -1;
		if(it->type == Job::READ && FD_ISSET(it->fd, rSet)) {
			n = read(it->fd, (char *)it->buf + it->total, it->n - it->total);
			
			if(n == 0) {
				(*it->callback)(it->callbackArg, 0);
				jobList.erase(it++);
				continue;
			}

		} else if(it->type == Job::WRITE && FD_ISSET(it->fd, wSet)) {
			n = it->n > 0 ?
					write(it->fd, (char *)it->buf + it->total, it->n - it->total)
					: 0;
		}

		if(n == -1 || (it->total += n) != it->n && it->type == Job::WRITE) {
			++it;
		} else {
			/* Job has finished */
			(*it->callback)(it->callbackArg, it->total);
			jobList.erase(it++);
		}
	}
}

void JobList::loop() {
	fd_set rSet, wSet;
	while(setCheckSet(&rSet, &wSet)) {
		select(maxFd + 1, &rSet, &wSet, NULL, NULL);
		checkDone(&rSet, &wSet);
	}
}
