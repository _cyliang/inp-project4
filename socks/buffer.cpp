#include "proxy.h"
#include <cstdlib>
using namespace std;

Proxy::Buffer::Buffer(): data(nullptr), size(0), consumed(0) {}

Proxy::Buffer::Buffer(void *_data, int n):
	data((char *) _data), size(n), consumed(0) {
}

Proxy::Buffer::Buffer(Buffer &&rhs):
	data(rhs.data), size(rhs.size), consumed(rhs.consumed) {

	rhs.data = nullptr;
	rhs.size = 0;
	rhs.consumed = 0;
}

Proxy::Buffer::~Buffer() {
	delete[] data;
}

void *Proxy::Buffer::getData() const {
	return data + consumed;
}

int Proxy::Buffer::getSize() const {
	return size - consumed;
}

void Proxy::Buffer::consume(int n) {
	consumed += n;
}
