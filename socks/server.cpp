#include "proxy.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
	if(argc != 3) {
		fprintf(stderr, "Usage: %s <port> <configuration_file>\n", argv[0]);
		exit(1);
	}

	Proxy::Configuration config(argv[2]);

	int listen_fd;
	struct sockaddr_in listen_addr;
	memset(&listen_addr, 0, sizeof(listen_addr));
	listen_addr.sin_family = AF_INET;
	listen_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	listen_addr.sin_port = htons(atoi(argv[1]));

	listen_fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
	if(listen_fd == -1) {
		fputs("Cannot create socket.\n", stderr);
		exit(2);
	}
	if(bind(listen_fd, (struct sockaddr *) &listen_addr, sizeof(listen_addr)) == -1) {
		fputs("Cannot bind.\n", stderr);
		exit(2);
	}
	if(listen(listen_fd, 1024) == -1) {
		fputs("Cannot listen.\n", stderr);
		exit(2);
	}

	int epoll_fd = epoll_create1(0);
	epoll_event listen_ev;

	listen_ev.events = EPOLLIN | EPOLLET;
	listen_ev.data.ptr = &listen_fd;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, listen_fd, &listen_ev);

	while(1) {
		epoll_event events[100];
		int count = epoll_wait(epoll_fd, events, 100, -1);

		for(int i=0; i<count; i++) {
			epoll_event &ev = events[i];

			if (ev.data.ptr == &listen_fd) {
				sockaddr_in addr;
				socklen_t len = sizeof(addr);

				int request_fd = accept(listen_fd, (sockaddr *) &addr, &len);
				assert(request_fd != -1);
				new Proxy(request_fd, epoll_fd, addr, config);
			} else {
				Proxy::Connection *conn = (Proxy::Connection *) ev.data.ptr;
				bool toDelete = false;

				if (ev.events & (EPOLLHUP | EPOLLRDHUP | EPOLLERR))
					toDelete |= conn->notifyClose();
				if (!toDelete && ev.events & EPOLLIN)
					toDelete |= conn->notifyRead();
				if (!toDelete && ev.events & EPOLLOUT)
					toDelete |= conn->notifyWrite();

				if (toDelete)
					delete &conn->proxy;
			}
		}
	}
}
