#pragma once
#include <cstdint>

struct SOCKS4 {
	uint8_t vn;

	uint8_t cd;

	union PORT {
		uint8_t byte[2];
		uint16_t word;
	} port;

	union IP {
		uint8_t byte[4];
		uint32_t dword;
	} ip;
};
