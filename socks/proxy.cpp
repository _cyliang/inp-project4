#include "proxy.h"
#include <netdb.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdlib>
#include <cstdio>
#include <cstring>
using namespace std;

Proxy::Proxy(int sock, int _epollfd, const sockaddr_in &addr, const Configuration &_config):
	requester(sock, REQUESTER, *this), remote(-1, REMOTE, *this),
	state(REQUESTING), epollfd(_epollfd), config(_config) {

	fillInfo(REQUESTER, addr);
	printIncoming();
}

void Proxy::fillInfo(Role role, const sockaddr_in &addr) {
	auto &info = role == REQUESTER ? requesterInfo : remoteInfo;

	memcpy(&info.networkByte.ip, &addr.sin_addr, sizeof(addr.sin_addr));
	memcpy(&info.networkByte.port, &addr.sin_port, sizeof(addr.sin_port));
	strcpy(info.ip, inet_ntoa(addr.sin_addr));
	info.port = ntohs(addr.sin_port);
}

void Proxy::notifyConnected(bool success, const sockaddr_in *addr) {
	if (success) {
		state = CONNECTED;
		if (addr) {
			fillInfo(REMOTE, *addr);
		}
		reply(true);
	} else {
		reply(false);
	}
}

void Proxy::request(Buffer &data) {
	new(&requestInfo.data) Buffer(move(data));

	requestInfo.info = (SOCKS4 *) requestInfo.data.getData();
	requestInfo.data.consume(sizeof(SOCKS4));

	if (requestInfo.data.getSize()) {
		requestInfo.userID = (char *) requestInfo.data.getData();
		requestInfo.data.consume(strlen(requestInfo.userID) + 1);
	} else {
		requestInfo.userID = NULL;
	}

	if (requestInfo.data.getSize()) {
		requestInfo.domainName = (char *) requestInfo.data.getData();
		requestInfo.data.consume(strlen(requestInfo.domainName) + 1);
	} else {
		requestInfo.domainName = NULL;
	}

	SOCKS4 &info = *requestInfo.info;
	if (info.vn != 0x04) {
		reply(false);
		return;
	}

	if (info.cd == 0x01) {
		mode = CONNECT;

		if (!info.ip.byte[0] && !info.ip.byte[1] && !info.ip.byte[2]) {
			hostent *host;
			if (!requestInfo.domainName || !(host = gethostbyname(requestInfo.domainName))) {
				notifyConnected(false);
				return;
			}
			memcpy(&info.ip, host->h_addr, sizeof(info.ip));
		}

		sockaddr_in addr;
		memset(&addr, 0x00, sizeof(addr));
		addr.sin_family = AF_INET;
		memcpy(&addr.sin_port, &info.port, sizeof(addr.sin_port));
		memcpy(&addr.sin_addr, &info.ip, sizeof(addr.sin_addr));

		fillInfo(REMOTE, addr);
		if (!checkPermit())
			return;

		int sock = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
		new(&remote) Connection(sock, REMOTE, *this);

		if (connect(sock, (sockaddr * ) &addr, sizeof(addr)) == 0) {
			notifyConnected(true);
		} else if (errno == EINPROGRESS) {
			state = CONNECTING;
		} else {
			notifyConnected(false);
		}
	} else if (info.cd == 0x02) {
		mode = BIND;

		sockaddr_in addr;
		memset(&addr, 0x00, sizeof(addr));
		addr.sin_family = AF_INET;
		addr.sin_port = 0;
		addr.sin_addr.s_addr = INADDR_ANY;

		fillInfo(REMOTE, addr);
		if (!checkPermit())
			return;

		int sock = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
		new(&remote) Connection(sock, REMOTE, *this);

		if (bind(sock, (sockaddr *) &addr, sizeof(addr)) != 0) {
			notifyConnected(false);
		} else if (listen(sock, 1) != 0) {
			notifyConnected(false);
		} else {
			state = ACCEPTING;
			
			socklen_t len = sizeof(addr);
			getsockname(sock, (sockaddr *) &addr, &len);
			memcpy(&info.port, &addr.sin_port, sizeof(info.port));
			memcpy(&info.ip, &addr.sin_addr, sizeof(info.ip));

			reply(true);
		}
	} else {
		reply(false);
	}
}

void Proxy::reply(bool accepted) {
	SOCKS4 *reply = new SOCKS4;

	memcpy(reply, requestInfo.info, sizeof(SOCKS4));
	reply->vn = 0x00;
	reply->cd = accepted ? 0x5A : 0x5B;

	Buffer buf(reply, sizeof(SOCKS4));
	requester.write(buf);

	if (!accepted) {
		requester.close(true);
		remote.close();
	}
}

bool Proxy::checkPermit() {
	int ruleLineNumber;
	const char *rule;

	if (config.check(mode, requesterInfo.networkByte.ip, requesterInfo.networkByte.port, remoteInfo.networkByte.ip, remoteInfo.networkByte.port, ruleLineNumber, rule)) {
		printAcceptedRejected(true, ruleLineNumber, rule);
		return true;
	} else {
		printAcceptedRejected(false, ruleLineNumber, rule);
		reply(false);
		return false;
	}
}

void Proxy::close() {
	requester.close();
	remote.close();
}

Proxy::State Proxy::getState() const {
	return state;
}

bool Proxy::isDead() const {
	return requester.isClosed() && remote.isClosed();
}

void Proxy::redirect(Proxy::Role role, Proxy::Buffer &buf) {
	Connection &target = role == REQUESTER ? remote : requester;

	printRedirect(role == REQUESTER ? REMOTE : REQUESTER, buf);
	target.write(buf);
}

void Proxy::printIncoming() const {
	printf("\e[33m******* Incoming request from \e[1m%15s/%-5hu\e[22m. *******\e[m\n", requesterInfo.ip, requesterInfo.port);
}

void Proxy::printAcceptedRejected(bool accepted, int ruleLineNumber, const char *rule) const {
	int color = accepted ? 32 : 31;

	printf("\e[%dm--- \e[1mRequest %-8s\e[22m ---------------------------------------\e[m\n", color, accepted ? "accepted" : "rejected");
	printf("\e[%dm|%-20s:\e[1m %-36s\e[22m|\e[m\n", color, "Requester IP", requesterInfo.ip);
	printf("\e[%dm|%-20s:\e[1m %-36hu\e[22m|\e[m\n", color, "Requester Port", requesterInfo.port);
	printf("\e[%dm|%-20s:\e[1m %-36s\e[22m|\e[m\n", color, "Requesting IP", remoteInfo.ip);
	printf("\e[%dm|%-20s:\e[1m %-36hu\e[22m|\e[m\n", color, "Requesting Port", remoteInfo.port);
	printf("\e[%dm|%-20s:\e[1m %-36s\e[22m|\e[m\n", color, "Command", mode == CONNECT ? "CONNECT" : "BIND");
	printf("\e[%dm|%-20s:\e[1m Rule of line %5d%-18s\e[22m|\e[m\n", color, "According to rule", ruleLineNumber, ":");
	printf("\e[%dm|    \e[m%50.50s\e[%dm    |\e[m\n", color, rule, color);
	printf("\e[%dm------------------------------------------------------------\e[m\n", color);
}

void Proxy::printRedirect(Role toward, const Buffer &content, bool accepted) const {
	auto &srcInfo = toward == REMOTE ? requesterInfo : remoteInfo;
	auto &dstInfo = toward == REMOTE ? remoteInfo : requesterInfo;
	int color = toward == REMOTE ? 35 : 36;

	printf("\e[%dm--- \e[1m%3s\e[22m ----------------------------------------------------\e[m\n", color, toward == REMOTE ? "OUT" : "IN");
	printf("\e[%dm|%-20s:\e[1m %-36s\e[22m|\e[m\n", color, "<S_IP>", srcInfo.ip);
	printf("\e[%dm|%-20s:\e[1m %-36hu\e[22m|\e[m\n", color, "<S_PORT>", srcInfo.port);
	printf("\e[%dm|%-20s:\e[1m %-36s\e[22m|\e[m\n", color, "<D_IP>", dstInfo.ip);
	printf("\e[%dm|%-20s:\e[1m %-36hu\e[22m|\e[m\n", color, "<D_PORT>", dstInfo.port);
	printf("\e[%dm|%-20s:\e[1m %-36s\e[22m|\e[m\n", color, "<COMMAND>", mode == CONNECT ? "CONNECT" : "BIND");
	printf("\e[%dm|%-20s:\e[1m %-36s\e[22m|\e[m\n", color, "<Reply>", accepted ? "Accept" : "Reject");

	int contentSize = content.getSize();
	if (contentSize) {
		contentSize = strcspn((char *) content.getData(), "\r\n");
	}

	printf("\e[%dm|%-20s:\e[1m %-33.*s%-3s\e[22m|\e[m\n", color, "<Content>",
		contentSize > 33 ? 33 : contentSize, contentSize ? content.getData() : "",
		contentSize > 33 ? "..." : "");
	printf("\e[%dm------------------------------------------------------------\e[m\n", color);
}
