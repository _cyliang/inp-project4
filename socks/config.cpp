#include "proxy.h"
#include <fstream>
#include <string>
#include <sstream>
using namespace std;

Proxy::Configuration::Configuration(const char *filename) throw(runtime_error) {
	ifstream confFile(filename, ios::in);
	if (!confFile)
		throw runtime_error(string("Invalid configuration file name: ") + filename);

	string ruleLine;
	int lineNumber = 0;
	while (getline(confFile, ruleLine)) {
		lineNumber++;
		istringstream iss(ruleLine);
		if (iss.peek() == '#')
			continue;

		Rule newRule;
		string token;
		if (! (iss >> token)) {
			continue;
		}
		if (token == "permit")
			newRule.permit = true;
		else if (token == "deny")
			newRule.permit = false;
		else
			throw runtime_error(string("Parsing configuration error: ") + ruleLine);

		iss >> token;
		if (token != "c" && token != "b")
			throw runtime_error(string("Parsing configuration error: ") + ruleLine);
		auto &ruleVector = token == "c" ? connectRule : bindRule;

		struct {
			Rule::IP &ip;
			Rule::PORT &port;
		} srcDst[2] {
			{newRule.srcIP, newRule.srcPort},
			{newRule.dstIP, newRule.dstPort}
		};
		for (auto &target : srcDst) {
			if (! (iss >> token)) {
				throw runtime_error(string("Parsing configuration error: ") + ruleLine);
			}

			if (token == "-") {
				for (int i=0; i<4; i++)
					target.ip.wildcard[i] = true;
			} else {
				istringstream ipStream(token);
				
				for (int i=0; i<4; i++) {
					if (ipStream.peek() == 'x') {
						target.ip.wildcard[i] = true;
						ipStream.get();
					} else {
						int portion;

						if (! (ipStream >> portion)) {
							throw runtime_error(string("Parsing configuration error: ") + ruleLine);
						}

						target.ip.ip.byte[i] = portion & 0xFF;
						target.ip.wildcard[i] = false;
					}

					ipStream.get();
				}
			}

			unsigned short port;
			if ((iss >> ws) && iss.peek() == '-') {
				target.port.wildcard = true;
				iss.get();
			} else if (! (iss >> port)) {
				throw runtime_error(string("Parsing configuration error: ") + ruleLine);
			} else {
				target.port.port.word = htons(port);
			}
		}

		newRule.lineNumber = lineNumber;
		newRule.ruleStr = ruleLine;
		ruleVector.push_back(newRule);
	}
}

bool Proxy::Configuration::check(Mode mode, SOCKS4::IP srcIP, SOCKS4::PORT srcPort, SOCKS4::IP dstIP, SOCKS4::PORT dstPort, int &lineNum, const char *&ruleStr) const {
	auto &checkVector = mode == CONNECT ? connectRule : bindRule;

	for (const Rule &rule : checkVector) {
		struct {
			const Rule::IP &ruleIP;
			const SOCKS4::IP &ip;
			const Rule::PORT &rulePort;
			const SOCKS4::PORT &port;
		} compares[2] {
			{rule.srcIP, srcIP, rule.srcPort, srcPort},
			{rule.dstIP, dstIP, rule.dstPort, dstPort}
		};
		bool match = true;

		for (auto &cmp : compares) {
			for (int i=0; i<4 && match; i++) {
				match &= cmp.ruleIP.wildcard[i] || cmp.ruleIP.ip.byte[i] == cmp.ip.byte[i];
			}
			match &= cmp.rulePort.wildcard || cmp.rulePort.port.word == cmp.port.word;
		}

		if (match) {
			lineNum = rule.lineNumber;
			ruleStr = rule.ruleStr.c_str();
			return rule.permit;
		}
	}

	lineNum = -1;
	ruleStr = "Denied by default rule.";
	return false;
}
