#pragma once
#include "socks4.h"
#include <queue>
#include <vector>
#include <string>
#include <stdexcept>
#include <netinet/in.h>

class Proxy {
public:
	enum Role { REQUESTER, REMOTE };
	enum State { REQUESTING, CONNECTING, ACCEPTING, CONNECTED };
	enum Mode { CONNECT, BIND };

	class Configuration {
	public:
		Configuration(const char *filename) throw(std::runtime_error);
		bool check(Mode mode, SOCKS4::IP srcIP, SOCKS4::PORT srcPort, SOCKS4::IP dstIP, SOCKS4::PORT dstPort, int &ruleLineNumber, const char *&ruleStr) const;

	private:
		struct Rule {
			bool permit;
			int lineNumber;
			std::string ruleStr;
			
			struct IP {
				SOCKS4::IP ip;
				bool wildcard[4];
			} srcIP, dstIP;

			struct PORT {
				SOCKS4::PORT port;
				bool wildcard;
			} srcPort, dstPort;
		};
		std::vector<Rule> bindRule, connectRule;
	};

	class Buffer {
	public:
		Buffer();
		Buffer(void *data, int size);
		Buffer(const Buffer &) = delete;
		Buffer(Buffer &&);
		~Buffer();

		void *getData() const;
		int getSize() const;
		void consume(int n);

	private:
		char *data;
		int size;
		int consumed;
	};

	class Connection {
	public:
		Connection(int fd, Role role, Proxy &proxy);
		bool notifyRead();
		bool notifyWrite();
		bool notifyClose();
		void write(Buffer &);
		void close(bool waitFinish = false);
		bool isClosed() const;

		Proxy &proxy;

	private:
		void setNonblockEpoll();
		Role role;
		int fd;
		bool toClose;

		std::queue<Buffer> writeBufs;
	};

	Proxy(int sock, int epollfd, const sockaddr_in &addr, const Configuration &config);
	State getState() const;
	bool isDead() const;
	void notifyConnected(bool, const sockaddr_in *addr = nullptr);
	void request(Buffer &);
	bool checkPermit();
	void reply(bool);
	void close();
	void redirect(Role, Buffer &);

	const int epollfd;

private:
	void fillInfo(Role role, const sockaddr_in &addr);
	void printIncoming() const;
	void printAcceptedRejected(bool accepted, int ruleLineNumber, const char *rule) const;
	void printRedirect(Role toward, const Buffer &, bool accepted = true) const;

	Mode mode;
	State state;
	Connection requester, remote;

	const Configuration &config;

	struct {
		Buffer data;

		SOCKS4 *info;
		char *userID;
		char *domainName;
	} requestInfo;

	struct {
		char ip[30];
		unsigned short port;

		struct {
			SOCKS4::IP ip;
			SOCKS4::PORT port;
		} networkByte;
	} requesterInfo, remoteInfo;
};
