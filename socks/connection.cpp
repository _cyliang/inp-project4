#include "proxy.h"
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stropts.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <cstdlib>
#include <cassert>
using namespace std;

Proxy::Connection::Connection(int _fd, Proxy::Role _role, Proxy &_proxy):
	fd(_fd), role(_role), proxy(_proxy), toClose(false) {
	
	setNonblockEpoll();
}

bool Proxy::Connection::notifyRead() {
	switch (proxy.getState()) {
		case ACCEPTING: {
			if (role == REQUESTER) {
				/* TODO: This is seen as an error usage. */
				break;
			}

			sockaddr_in addr;
			socklen_t len = sizeof(addr);
			int newFd = ::accept(fd, (sockaddr *) &addr, &len);
			if (newFd >= 0) {
				::close(fd);
				fd = newFd;
				setNonblockEpoll();
				proxy.notifyConnected(true, &addr);
			} else if (errno != EWOULDBLOCK) {
				proxy.notifyConnected(false);
			}
			break;
		}

		case CONNECTING:
			if (role == REQUESTER) {
				/* TODO: This is seen as an error usage. */
				break;
			}
			break;

		case REQUESTING:
		case CONNECTED: {
			int numRead;
			do {
				ioctl(fd, FIONREAD, &numRead);
				if (numRead == 0)
					break;

				char *content = new char[numRead];
				numRead = ::read(fd, content, numRead);

				if (numRead == -1 && errno != EWOULDBLOCK) {
					/* Read Error */
				} else if (numRead > 0) {
					Buffer buf(content, numRead);

					if (proxy.getState() == REQUESTING) {
						proxy.request(buf);
						break;
					} else
						proxy.redirect(role, buf);
					
					continue;
				}

				delete[] content;
			} while (numRead != -1);
			break;
		}
	}

	return proxy.isDead();
}

bool Proxy::Connection::notifyWrite() {
	switch (proxy.getState()) {
		case CONNECTING: {
			int err;
			socklen_t err_len = sizeof(err);
			getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &err_len);

			proxy.notifyConnected(err == 0);
			break;
		}
		
		case REQUESTING:	
		case ACCEPTING:
		case CONNECTED:
			if (!writeBufs.empty()) {
				Buffer &buf = writeBufs.front();
				int n = ::write(fd, buf.getData(), buf.getSize());

				if (n == -1 && errno != EWOULDBLOCK) {
					/* Write error */
				} else if (n > 0) {
					buf.consume(n);

					if (!buf.getSize()) {
						writeBufs.pop();

						if (toClose && writeBufs.empty())
							close(false);
					}
				}
			}
			break;
	}

	return proxy.isDead();
}

bool Proxy::Connection::notifyClose() {
	proxy.close();
	return proxy.isDead();
}

void Proxy::Connection::write(Proxy::Buffer &buf) {
	writeBufs.push(move(buf));
	notifyWrite();
}

void Proxy::Connection::close(bool waitFinish) {
	if (!waitFinish || writeBufs.empty()) {
		::close(fd);
		fd = -1;
	} else
		toClose = true;
}

bool Proxy::Connection::isClosed() const {
	return fd == -1;
}

void Proxy::Connection::setNonblockEpoll() {
	if (fd == -1)
		return;

	int flags = fcntl(fd, F_GETFL, 0);
	fcntl(fd, F_SETFL, flags | O_NONBLOCK);

	epoll_event event;
	event.events = EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLET;
	event.data.ptr = this;
	if (epoll_ctl(proxy.epollfd, EPOLL_CTL_ADD, fd, &event) == -1)
		assert_perror(errno);
}
